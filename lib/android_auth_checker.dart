import 'dart:async';

import 'package:flutter/services.dart';

class AndroidAuthChecker {
  static const MethodChannel _channel =
      const MethodChannel('android_auth_checker');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<bool> canCheckBiometrics() async {
    return await _channel.invokeMethod('canCheckBiometrics');
  }
}
