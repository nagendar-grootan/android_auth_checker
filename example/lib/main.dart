import 'dart:developer';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:android_auth_checker/android_auth_checker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String biometrics = '';

  @override
  void initState() {
    super.initState();
    canCheckBiometrics();
  }

  canCheckBiometrics() async {
    bool result = await AndroidAuthChecker.canCheckBiometrics();
    setState(() {
      biometrics = "$result";
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Android Auth Checker'),
        ),
        body: Center(
          child: Text('Biometrics response: $biometrics\n'),
        ),
      ),
    );
  }
}
