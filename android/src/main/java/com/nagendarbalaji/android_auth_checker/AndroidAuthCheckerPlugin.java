package com.nagendarbalaji.android_auth_checker;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.biometric.BiometricManager;
import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** AndroidAuthCheckerPlugin */
public class AndroidAuthCheckerPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private Context context;


  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    System.out.println("Attached to Engine");
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "android_auth_checker");
    channel.setMethodCallHandler(this);
    context = flutterPluginBinding.getApplicationContext();
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    } else if(call.method.equals("canCheckBiometrics")) {
      result.success(canCheckBiometrics());
    }
    else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  @SuppressLint("WrongConstant")
  public Boolean canCheckBiometrics() {
    BiometricManager manager = BiometricManager.from(context);
//    int i = manager.canAuthenticate(BiometricManager.Authenticators.DEVICE_CREDENTIAL);
//    int j = manager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG);
//    int k = manager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK);
//    int l = manager.canAuthenticate();
//    System.out.println("Device Cred" + i);
//    System.out.println("Bio Strong" + j);
//    System.out.println("Bio Weak" + k);
//    System.out.println("Old" + l);
    return (manager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) != -1);
  }
}
